package com.eva.core.exception;

/**
 * 不安全的会话异常
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
public class UnSafeSessionException extends RuntimeException {

    public UnSafeSessionException () {
        super("不安全的会话");
    }
}
