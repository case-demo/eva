package com.eva.service.system;

import com.eva.core.model.PageData;
import com.eva.core.model.PageWrap;
import com.eva.dao.system.model.SystemRolePermission;
import java.util.List;

/**
 * 角色权限关联Service定义
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
public interface SystemRolePermissionService {

    /**
     * 创建
     *
     * @param systemRolePermission 实体
     * @return Integer
     */
    Integer create(SystemRolePermission systemRolePermission);

    /**
     * 主键删除
     *
     * @param id 主键
     */
    void deleteById(Integer id);

    /**
     * 删除
     *
     * @param systemRolePermission 删除条件
     */
    void delete(SystemRolePermission systemRolePermission);

    /**
     * 批量主键删除
     *
     * @param ids 主键列表
     */
    void deleteByIdInBatch(List<Integer> ids);

    /**
     * 主键更新
     *
     * @param systemRolePermission 实体
     */
    void updateById(SystemRolePermission systemRolePermission);

    /**
     * 批量主键更新
     *
     * @param systemRolePermissions 实体列表
     */
    void updateByIdInBatch(List<SystemRolePermission> systemRolePermissions);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return SystemRolePermission
     */
    SystemRolePermission findById(Integer id);

    /**
     * 条件查询单条记录
     *
     * @param systemRolePermission 查询条件
     * @return SystemRolePermission
     */
    SystemRolePermission findOne(SystemRolePermission systemRolePermission);

    /**
     * 条件查询
     *
     * @param systemRolePermission 查询条件
     * @return List<SystemRolePermission>
     */
    List<SystemRolePermission> findList(SystemRolePermission systemRolePermission);

    /**
     * 分页查询
     *
     * @param pageWrap 分页参数
     * @return PageData<SystemRolePermission>
     */
    PageData<SystemRolePermission> findPage(PageWrap<SystemRolePermission> pageWrap);

    /**
     * 条件统计
     *
     * @param systemRolePermission 统计参数
     * @return long
     */
    long count(SystemRolePermission systemRolePermission);
}
