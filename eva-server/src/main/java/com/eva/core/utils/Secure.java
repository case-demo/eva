package com.eva.core.utils;

import com.alibaba.fastjson.JSON;
import com.eva.core.model.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * 安全处理工具类
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
@Slf4j
@Component
public class Secure {

    /**
     * 加密密码
     *
     * @param password 密码
     * @param salt 密码盐
     * @return String
     */
    public String encryptPassword(String password, String salt) {
        return this.encryptMD5Password(DigestUtils.md5DigestAsHex(password.getBytes()), salt);
    }

    /**
     * 加密密码
     *
     * @param md5Password 密码
     * @param salt 密码盐
     * @return String
     */
    public String encryptMD5Password(String  md5Password, String salt) {
        return DigestUtils.md5DigestAsHex((md5Password + salt).getBytes());
    }
}
